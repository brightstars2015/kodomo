<?php include('header.php'); ?>
<body class="shop">
<?php include('menu.php'); ?>
<div class="wrapper">
	<!-- <div class="bunga-container bunga kodomo-product-kelinci">
		<img src="assets/img/kodomo-produk-kelinci.png" />
    </div>
    <div class="bunga-container bunga kodomo-product-jerapah">
    	<img src="assets/img/kodomo-produk-jerapah.png" />
    </div>
    <div class="bunga-container bunga kodomo-product-beruang">
    	<img src="assets/img/kodomo-produk-beruang.png" />
    </div>
    <div class="bunga-container bunga kodomo-product-gajah">
    	<img src="assets/img/kodomo-produk-gajah.png" />
    </div>
    <div class="bunga-container bunga kodomo-product-tiger">
    	<img src="assets/img/kodomo-produk-tiger.png" />
    </div> -->
	<div class="container">
		<div class="content">
			<div class="row">
				<div class="col-lg-12 title-shop"><img src="assets/img/kodomo-kids.png" /></div>
			</div>
			<div class="clear"></div>
			<br>
			<div class="row">
				<div class="col-lg-4">
					<div class="tvc">
						<!-- <img src="assets/img/tvc.png" /> -->
						<ul>
							<li><a class="popup-tvc" data-url="https://www.youtube.com/embed/MtgejRoKr98?enablejsapi=1&version=3&playerapiid=youtubevideo&autoplay=0&rel=0" href="#">Kodomo Fooming</a></li>
							<li><a class="popup-tvc" href="#">Kodomo Shampoo</a></li>
							<li><a class="popup-tvc" href="#">Kodomo Bodywash</a></li>
							<li><a class="popup-tvc" href="#">Kodomo Kiddy</a></li>
							<li><a class="popup-tvc" href="#">Kodomo Tooth</a></li>
							<li><a class="popup-tvc" href="#">Kodomo Funny</a></li>
							<li><a class="popup-tvc" href="#">Kodomo Fooming</a></li>
							<li><a class="popup-tvc" href="#">Kodomo Shampoo</a></li>
							<li><a class="popup-tvc" href="#">Kodomo Bodywash</a></li>
							<li><a class="popup-tvc" href="#">Kodomo Kiddy</a></li>
							<li><a class="popup-tvc" href="#">Kodomo Tooth</a></li>
							<li><a class="popup-tvc" href="#">Kodomo Funny</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-7 product-container">
					<ul id="product">
						<li class="product-child">
							<div class="article">
			                  <span class="title" title="">KODOMO FOAMING</span>
			                </div>
							<div class="box-image"><img src="assets/img/kodomo-body-wash.png" /></div>
			                <div class="button-link"><a data-url="assets/img/kodomo-body-wash.png" data-title="Kodomo Bodywash" data-content="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua" class="popup" href="#">MORE INFO</a></div>
						</li>
						<li class="product-child">
							<div class="article">
			                  <span class="title" title="">KODOMO FOAMING</span>
			                </div>
							<div class="box-image"><img src="assets/img/kodomo-produk.png" /></div>
			                <div class="button-link"><a data-url="assets/img/kodomo-produk.png" data-title="Kodomo Wash" data-content="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est " class="popup" href="#">MORE INFO</a></div>
						</li>
						<li class="product-child">
							<div class="article">
			                  <span class="title" title="">KODOMO FOAMING</span>
			                </div>
							<div class="box-image"><img src="assets/img/kodomo-body-wash.png" /></div>
			                <div class="button-link"><a data-url="assets/img/kodomo-body-wash.png" data-title="Kodomo Bodywash" data-content="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam" class="popup" href="#">MORE INFO</a></div>
						</li>
						<li class="product-child">
							<div class="article">
			                  <span class="title" title="">KODOMO FOAMING</span>
			                </div>
							<div class="box-image"><img src="assets/img/kodomo-produk.png" /></div>
			                <div class="button-link"><a data-url="assets/img/kodomo-produk.png" data-title="Kodomo Wash" data-content="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua" class="popup" href="#">MORE INFO</a></div>
						</li>
						<li class="product-child">
							<div class="article">
			                  <span class="title" title="">KODOMO FOAMING</span>
			                </div>
							<div class="box-image"><img src="assets/img/kodomo-produk.png" /></div>
			                <div class="button-link"><a data-url="assets/img/kodomo-produk.png" data-title="Kodomo Wash" data-content="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua" class="popup" href="#">MORE INFO</a></div>
						</li>
						<li class="product-child">
							<div class="article">
			                  <span class="title" title="">KODOMO FOAMING</span>
			                </div>
							<div class="box-image"><img src="assets/img/kodomo-produk.png" /></div>
			                <div class="button-link"><a data-url="assets/img/kodomo-produk.png" data-title="Kodomo Wash" data-content="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua" class="popup" href="#">MORE INFO</a></div>
						</li>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="popupbox" id="popup1">
	<div class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-4"><img src="assets/img/kodomo-produk.png" /></div>
					<div class="col-lg-6">
						<div class="title">Kodomo Bodywash</div>
						<div class="article">
							Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a class="close-popup" href="#"></a>
</div>

<div class="popupbox" id="popup-tvc">
	<img src="assets/img/bg-tvc.png" />
	<div class="content">
		<div class="row">
			<div class="col-lg-12 frame">

			</div>
			<a class="play-button" href="#"></a>
		</div>
	</div>
	<a class="close-popup" href="#"></a>
</div>

<?php include('footer.php'); ?>

<script type="text/javascript">
	var player = '';
	$(document).ready(function(){
		$('#navbar ul.menu li.has-sub').hover(function(){
	      $(this).toggleClass('expanded');
	    });

	    $('a.popup').click(function() {
			// var popupid = $(this).attr('rel');
			$('#popup1 .content img').attr('src', $(this).attr('data-url'));
			$('#popup1 .content .title').html($(this).attr('data-title'));
			$('#popup1 .content .article').html($(this).attr('data-content'));

			$('#popup1').fadeIn();

			$('body').append('<div id="fade" onClick="$(\'#fade, .popupbox\').fadeOut(); $(\'#fade\').remove(); return false;"></div>');
			$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn();

			var popuptopmargin = ($('#popup1').height() + 10) / 2;
			var popupleftmargin = ($('#popup1').width() + 10) / 2;

			$('#popup1').css({
				'margin-top' : -popuptopmargin,
				'margin-left' : -popupleftmargin
			});
		});

		$('.close-popup').click(function(){
			$('#fade, .popupbox').fadeOut();
			$('#fade').remove();
			player.stopVideo();
			return false;
		});
	});
	$(window).load(function() {
		$("#product").flexisel({
    		visibleItems: 2,
    		clone:false,
		    enableResponsiveBreakpoints: true,
		    responsiveBreakpoints: {
		      portrait: {
		        changePoint:480,
		        visibleItems: 1
		      }, 
		      landscape: {
		        changePoint:640,
		        visibleItems: 2
		      },
		      tablet: {
		        changePoint:768,
		        visibleItems: 3
		      }
		    }
    	});
	});

	$(function() {
		// Load the YouTube API. For some reason it's required to load it like this
		var tag = document.createElement('script');
	    tag.src = "//www.youtube.com/iframe_api";
	    var firstScriptTag = document.getElementsByTagName('script')[0];
	    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	 	
	    function onPlayerReady(event){
	    	event.target.playVideo();
	    }

	    // Watch for changes on the player
	    function onStateChange(state){
			switch(state.data)
			{
				// If the user is playing a video, stop the slider
				case YT.PlayerState.PLAYING:
					break;
				// The video is no longer player, give the go-ahead to start the slider back up
				case YT.PlayerState.ENDED:
					$('.play-button').show();
					break;
				case YT.PlayerState.PAUSED:
					break;
			}
		}

		$('a.popup-tvc').click(function() {
			// var popupid = $(this).attr('rel');
			var html = '<iframe id="player" src="'+ $(this).attr('data-url') +'" width="460" height="400" frameborder="0" allowfullscreen="" />'
			$('#popup-tvc .content .frame').html(html);

			player = new YT.Player('player', {
				playerVars: {
					autoplay: 0
				},
				events:{
					'onStateChange'	: onStateChange
				}
			});
			$('.play-button').show();

			$('#popup-tvc').fadeIn();

			$('body').append('<div id="fade" onClick="$(\'#fade, .popupbox\').fadeOut(); $(\'#fade\').remove(); player.stopVideo(); return false;"></div>');
			$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn();

			var popuptopmargin = ($('#popup-tvc').height() + 10) / 2;
			var popupleftmargin = ($('#popup-tvc').width() + 10) / 2;

			$('#popup-tvc').css({
				'margin-top' : -popuptopmargin,
				'margin-left' : -popupleftmargin
			});
		});

		$('.play-button').click(function(){
			$(this).fadeOut();
			player.playVideo();
		});
	});
</script>