<?php include('header.php'); ?>

<body class="health">

<?php include('menu.php'); ?>
<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 vaksin-container">
				<ul class="list-vaksin">
					<li><a data-toggle="tooltip" data-placement="left" data-original-title="Bacille Calmette-Guérin (BCG) adalah vaksin untuk tuberkulosis yang dibuat dari baksil tuberkulosis (Mycobacterium bovis) yang dilemahkan dengan dikulturkan di medium buatan selama bertahun-tahun. Vaksin BCG 80% efektif dapat mencegah selama 15 tahun, tetapi efeknya bervariasi tergantung kepada kondisi geografis." href="#">BCG</a></li>
					<li><a data-toggle="tooltip" data-placement="left" data-original-title="Penyakit Hepatitis B merupakan penyakit yang dapat ditularkan melalui hubungan seksual dari cairan sperma (sexual transmitted disease/STD), darah atau cairan tubuh lain. Penyakit ini dapat menimbulkan penyakit serius dan mengakibatkan kerusakan hati yang dapat berakhir dengan kematian atau kanker hati. Hepatitis B terdiri dari antigen permukaan (surface antigen) yang disebut dengan antigen Australia, karena antigen ini pertama kali dijumpai di Australia." href="#">Hepatitis B</a></li>
					<li><a data-toggle="tooltip" data-placement="left" data-original-title="Poliomielitis atau polio, adalah penyakit paralisis atau lumpuh yang disebabkan oleh virus. Agen pembawa penyakit ini, sebuah virus yang dinamakan poliovirus (PV), masuk ke tubuh melalui mulut, mengifeksi saluran usus. Virus ini dapat memasuki aliran darah dan mengalir ke sistem saraf pusat menyebabkan melemahnya otot dan kadang kelumpuhan (paralisis)." href="#">Polio</a></li>
					<li><a data-toggle="tooltip" data-placement="left" data-original-title="Pentabio adalah Vaksin DTP-HB-Hib (Vaksin Jerap Difteri, Tetanus, Pertusis, Hepatitis B Rekombinan, Haemophilus influenzae tipe b) berupa suspensi homogen yang mengandung toksoid tetanus dan difter-i murni, bakter-i pertusis (batuk rejan) inaktif,antigen permukaan hepatitis B (HBsAg) murni yang tidak infeksius, dan komponen Hib sebagai vaksin bakteri sub unit berupa kapsul polisakarida Haemophilus influenzae tipe b tidak infeksius yang dikonjugasikan kepada protein toksoid tetanus." href="#">DTP</a></li>
					<li><a data-toggle="tooltip" data-placement="left" data-original-title="Pengertian Imunisasi Dasar - Imunisasi adalah upaya yang dilakukan dengan sengaja memberikan kekebalan (imunisasi ) pada bayi atau anak sehingga terhindar dari penyakit (Supartini, Y, 2004)." href="#">Campak</a></li>
					<li><a data-toggle="tooltip" data-placement="left" data-original-title="Haemophilus influenzae tipe B adalah kuman penyebab UTAMA pneumonia (radang paru) dan meningitis (radang selaput otak) pada anak berumur kurang dari 5 tahun. WHO menyatakan bahwa saat ini Hib merupakan penyebab dari 3 juta kasus meningitis dan pneumonia di dunia, dengan 400.000 kematian setiap tahunnya." href="#">Hib</a></li>
					<li><a data-toggle="tooltip" data-placement="left" data-original-title="Lorem ipsum" href="#">Pneumokokus</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php include('footer.php'); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#navbar ul.menu li.has-sub').hover(function(){
	      $(this).toggleClass('expanded');
	    });
	    $('.list-vaksin li a').tooltip();
	});
</script>
