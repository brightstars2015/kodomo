<html>
	<head>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
		<script src="assets/js/jquery.carouFredSel-6.2.1-packed.js"></script>
	</head>
	<body>
		<div id="carousel">
		    <div>
		        <h3>Infinity</h3>
		        <p>A concept that in many fields refers to a quantity without bound or end.</p>
		    </div>
		    <div>
		        <h3>Circular journey</h3>
		        <p>An excursion in which the final destination is the same as the starting point.</p>
		    </div>
		    <div>
		        <h3>jQuery</h3>
		        <p>jQuery  is a cross-browser JavaScript library designed to simplify the client-side scripting.</p>
		    </div>
		    <div>
		        <h3>Infinity</h3>
		        <p>A concept that in many fields refers to a quantity without bound or end.</p>
		    </div>
		    <div>
		        <h3>Circular journey</h3>
		        <p>An excursion in which the final destination is the same as the starting point.</p>
		    </div>
		    <div>
		        <h3>jQuery</h3>
		        <p>jQuery  is a cross-browser JavaScript library designed to simplify the client-side scripting.</p>
		    </div>
		    <div>
		        <h3>Infinity</h3>
		        <p>A concept that in many fields refers to a quantity without bound or end.</p>
		    </div>
		    <div>
		        <h3>Circular journey</h3>
		        <p>An excursion in which the final destination is the same as the starting point.</p>
		    </div>
		    <div>
		        <h3>jQuery</h3>
		        <p>jQuery  is a cross-browser JavaScript library designed to simplify the client-side scripting.</p>
		    </div>
		</div>
		<script>
		$(document).ready(function(){
			$('#carousel').carouFredSel({
				items: 4,
				direction           : "left",
		        scroll : {
		            items           : 1,
		            easing          : "elastic",
		            duration        : 1000,                         
		            pauseOnHover    : true
		        }   
			});
		});
		</script>
	</body>
</html>