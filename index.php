<?php include('header.php'); ?>
<body class="home">
  <?php include('menu.php'); ?>
  <div class="wrapper">
    <div class="bunga-container bunga bunga-1">
      <img src="assets/img/bunga-1.png" />
    </div>
    <div class="bunga-container bunga bunga-2">
      <img src="assets/img/bunga-1.png" />
    </div>
    <div class="bunga-container bunga-banyak">
      <img src="assets/img/bunga-banyak.png" />
    </div>
    <div class="bunga-container balon-udara">
      <img src="assets/img/balon-udara.png" />
    </div>
    <div class="bunga-container balon-udara-small">
      <img src="assets/img/balon-udara-small.png" />
    </div>

    <div class="ovalContainer">
      <div class="oval-bg">
        <ul>
          <li class="child item_1">
            <div class="buildingContainer">
              <div class="buildingImg" style="margin-left: -277px;">
                  <a href="#"></a>
                  <!-- <div class="buildingTooltip" style="top: -150px; left: 30px; display: none;">
                      <img alt="Apps" src="http://www.pocarisweat.co.id/userdata/navigation/ac5d2d19208120bd7084648140f669c7.png" data-url="http://www.pocarisweat.co.id/userdata/navigation/ac5d2d19208120bd7084648140f669c7.png">
                  </div> -->
              </div>
            </div>
            <div class="landingNavBg">
                <div class="landingNavBgImg">
                  <!-- <div class="feature-kids-button"><a href="#"><img src="assets/img/up.png" /></a><span>FEATURE KIDS</span></div> -->
                  <img src="assets/img/full-kids.png" style="">
                </div>
            </div>
          </li>
          <!-- <li><div class="kids"><div class="kidsImg"><a><img src="assets/img/kids.png" /></a></div></div></li> -->
        </ul>
      </div>
    </div>

    <div class="ovalContainer" id="feature">
      <div class="toggle">
        <div class="title-feature closed">
          <a href="#"><img src="assets/img/up.png" /></a><span>FEATURE KIDS</span>
        </div>
        <div class="title-feature open">
          <a href="#"><img src="assets/img/down.png" /></a><span>FEATURE KIDS</span>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="oval-bg">
        <div class="container">
          <ul id="flexisel">
              <li class="kids">
                <div class="box-image"><img src="assets/img/birthday-kids.png" /></div>
                <div class="article">
                  <div class="title">Birthday Kids</div>
                  <div class="excerpt">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</div>
                </div>
              </li>
              <li class="kids">
                <div class="box-image"><img src="assets/img/fun-corner.png" /></div>
                <div class="article">
                  <div class="title">Fun Corner</div>
                  <div class="excerpt">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</div>
                </div>
              </li>
              <li class="kids">
                <div class="box-image"><img src="assets/img/diary-kids.png" /></div>
                <div class="article">
                  <div class="title">Diary Kids</div>
                  <div class="excerpt">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</div>
                </div>
              </li>
              <li class="kids">
                <div class="box-image"><img src="assets/img/puskesmas.png" /></div>
                <div class="article">
                  <div class="title">Puskesmas</div>
                  <div class="excerpt">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</div>
                </div>
              </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="clouds"></div>
</div>
<?php include('footer.php'); ?>
<script type="text/javascript">
  $(document).ready(function(){
    //animate background
    // var to;
    // $('.clouds').css({backgroundPosition: '0px 0px'});
    // function infinite(){
    //   setTimeout(function(){
    //     $('.clouds').animate({backgroundPosition:"-12000px 0"}, 1000,function(){
    //       $('.clouds').css({backgroundPosition:'0px 0px'});
    //       infinite();
    //     });    
    //   });
    // }
    // infinite();

    $('.bunga').jqFloat({
      width:200,
      height:250,
      speed:1200
    });

    $('.bunga-banyak').jqFloat({
      width:50,
      height:50,
      speed:1200
    });

    $('.balon-udara').jqFloat({
      width:100,
      height:300,
      speed:1500,
      minHeight:30
    });

    $('.balon-udara-small').jqFloat({
      width:200,
      height:250,
      speed:1200
    });

    var closed = 0;
    $('#feature .toggle .closed a img').click(function(){
      $('#feature .toggle .closed').hide();
      $('#feature .oval-bg').css({'padding-top':'90px','height':'486px', "border-top": "#fff solid"}).addClass('bounceInUp animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $('#feature .toggle .open').show();
        // $('#feature').addClass('expanded');
        $(this).removeClass('bounceInUp animated');
      });
      return false;
    });
    $('#feature .toggle .open a img').click(function(){
      $('#feature .toggle .open').hide();
      $('#feature .oval-bg').addClass('bounceOutDown animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $('#feature .toggle .closed').show();
        $(this).css({'padding-top':'0px','height':'0px', "border-top": "none"});
        // $('#feature').removeClass('expanded');
        $(this).removeClass('bounceOutDown animated');
      });
      return false;
    });
    $('#navbar ul.menu li.has-sub').hover(function(){
      $(this).toggleClass('expanded');
    });
  });

  $(window).load(function() {
    $("#flexisel").flexisel();
  });
</script>