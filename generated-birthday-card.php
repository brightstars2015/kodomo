<?php 
	include 'library/html2pdf/html2pdf.class.php';
	$html_content = file_get_contents('http://127.0.0.1/brightstars/kodomo/birthday-card.php');

	$html2pdf = new HTML2PDF('P', 'A4', 'en');
	$html2pdf->writeHTML($html_content);
	$file = $html2pdf->Output('temp.pdf','F');

	$im = new imagick('temp.pdf');
	$im->setImageFormat( "jpg" );
	$img_name = time().'.jpg';
	$im->setSize(1366,667);
	$im->writeImage($img_name);
	$im->clear();
	$im->destroy();
?>