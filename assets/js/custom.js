$(document).ready(function() {
	$('a.popup').click(function() {
		// var popupid = $(this).attr('rel');
		$('#popup1 .content img').attr('src', $(this).attr('data-url'));
		$('#popup1 .content .title').html($(this).attr('data-title'));
		$('#popup1 .content .article').html($(this).attr('data-content'));

		$('#popup1').fadeIn();

		$('body').append('<div id="fade" onClick="$(\'#fade, .popupbox\').fadeOut(); return false;"></div>');
		$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn();

		var popuptopmargin = ($('#popup1').height() + 10) / 2;
		var popupleftmargin = ($('#popup1').width() + 10) / 2;

		$('#popup1').css({
			'margin-top' : -popuptopmargin,
			'margin-left' : -popupleftmargin
		});
	});

	$('.close-popup').click(function(){
		$('#fade, .popupbox').fadeOut();
		return false;
	});

	$('#fade').click(function() {
		$('#fade, .popupbox').fadeOut();
		return false;
	});
});