<!-- DESKTOP -->
<div id="navbar">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 desktop">
        <div class="pull-left"><img src="assets/img/bg-menu.png" class="logo"/></div>
        <div class="pull-left menu-container">
          <ul class="menu">
            <li class="has-sub" id="first-menu">
              <a href="index.php">MENU</a>
              <div class="sub-menu">
                <ul>
                  <li class="sub-menu-child birthday"><a href="birthday.php">Birthday Banner</a></li>
                  <li class="sub-menu-child"><a href="health.php">Puskesmas</a></li>
                  <li class="sub-menu-child"><a href="character.php">Kodomo's Character</a></li>
                  <li class="sub-menu-child"><a href="fun-corner.php">Fun Corner</a></li>
                  <li class="sub-menu-child"><a href="#">Kodomo's Diary</a></li>
                </ul>
              </div>
            </li>
            <li id="second-menu"><a href="shop.php">KODOMO KIDS</a></li>
            <li id="third-menu"><a href="#">REGISTER</a></li>
          </ul>
        </div>
        <div class="pull-left share-container">
          <div id="share">
            <a class="icons facebook" href=""></a>
            <a class="icons twit" href=""></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END DESKTOP -->