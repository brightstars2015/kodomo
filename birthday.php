<?php include('header.php'); ?>

<body class="birthday">

<?php include('menu.php'); ?>

<div class="wrapper">
	<!-- <div class="bunga-container bunga birthday-tiger">
		<img src="assets/img/birthday-tiger.png" />
    </div>
    <div class="bunga-container bunga birthday-gajah">
		<img src="assets/img/birthday-gajah.png" />
    </div>
    <div class="bunga-container bunga birthday-beruang">
		<img src="assets/img/birthday-beruang.png" />
    </div>
    <div class="bunga-container bunga birthday-jerapah">
		<img src="assets/img/birthday-jerapah.png" />
    </div> -->

	<div class="container">
		<div class="content">
			<div class="row">
				<div class="col-lg-9 list-container">
					<ul id="list-anak">
						<li class="content-anak" data-url="assets/img/fayza.png" data-nickname="Fayza Firdaus" data-age="5">
							<div class="box-image popup" style="background-image: url(assets/img/fayza.png); ">	</div>
			                <div class="button-name popup"><a href="#">Fayza Firdaus</a></div>
						</li>
						<li class="content-anak" data-url="assets/img/indra-samosa.png" data-nickname="Indra Samosa" data-age="6">
							<div class="box-image popup" style="background-image: url(assets/img/indra-samosa.png); "></div>
			                <div class="button-name popup"><a href="#">Indra Samosa</a></div>
						</li>
						<li class="content-anak" data-url="assets/img/putu-ananta.png" data-nickname="Putu Ananta" data-age="7">
							<div class="box-image popup" style="background-image: url(assets/img/putu-ananta.png); "></div>
			                <div class="button-name popup"><a href="#">Putu Ananta</a></div>
						</li>
						<li class="content-anak" data-url="assets/img/artiliani.png" data-nickname="Artiliani" data-age="8">
							<div class="box-image popup" style="background-image: url(assets/img/artiliani.png); "></div>
			                <div class="button-name popup"><a href="#">Artiliani</a></div>
						</li>
						<li class="content-anak" data-url="assets/img/fayza.png" data-nickname="Fayza Firdaus" data-age="5">
							<div class="box-image popup" style="background-image: url(assets/img/fayza.png); ">	</div>
			                <div class="button-name popup"><a href="#">Fayza Firdaus</a></div>
						</li>
						<li class="content-anak" data-url="assets/img/indra-samosa.png" data-nickname="Indra Samosa" data-age="6">
							<div class="box-image popup" style="background-image: url(assets/img/indra-samosa.png); "></div>
			                <div class="button-name popup"><a href="#">Indra Samosa</a></div>
						</li>
						<li class="content-anak" data-url="assets/img/putu-ananta.png" data-nickname="Putu Ananta" data-age="7">
							<div class="box-image popup" style="background-image: url(assets/img/putu-ananta.png); "></div>
			                <div class="button-name popup"><a href="#">Putu Ananta</a></div>
						</li>
						<li class="content-anak" data-url="assets/img/artiliani.png" data-nickname="Artiliani" data-age="8">
							<div class="box-image popup" style="background-image: url(assets/img/artiliani.png); "></div>
			                <div class="button-name popup"><a href="#">Artiliani</a></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="popupbox" id="popup-birthday">
	<div class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-4 img-anak" style="background-image: url(assets/img/fayza.png); "></div>
					<div class="col-lg-6">
						<div class="title">Selamat ulang tahun yang ke <span class="age">8</span> , <span class="nickname">Putu Anjani</span></div>
						<div class="share-card">Share birthday card</div>
						<a class="icon twitter"></a><a class="icon fb"></a>
					</div>
				</div>
			</div>
			<a class="play-button" href="#"></a>
		</div>
	</div>
	<a class="close-popup" href="#"></a>
</div>

<?php include('footer.php'); ?>

<script type="text/javascript">
	var player = '';
	$(document).ready(function(){
		$('#navbar ul.menu li.has-sub').hover(function(){
	      $(this).toggleClass('expanded');
	    });

	    $('.popup').click(function() {
			// var popupid = $(this).attr('rel');
			$('#popup-birthday .content .img-anak').css('background-image', 'url(' + $(this).parent().attr('data-url') + ')');
			$('#popup-birthday .content .title span.nickname').html($(this).parent().attr('data-nickname'));
			$('#popup-birthday .content .title span.age').html($(this).parent().attr('data-age'));

			$('#popup-birthday').fadeIn();

			$('body').append('<div id="fade" onClick="$(\'#fade, .popupbox\').fadeOut(); $(\'#fade\').remove(); return false;"></div>');
			$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn();

			var popuptopmargin = ($('#popup-birthday').height() + 10) / 2;
			var popupleftmargin = ($('#popup-birthday').width() + 10) / 2;

			$('#popup-birthday').css({
				'margin-top' : -popuptopmargin,
				'margin-left' : -popupleftmargin
			});
		});

		$('.close-popup').click(function(){
			$('#fade, .popupbox').fadeOut();
			$('#fade').remove();
			return false;
		});

	    $(window).load(function() {
			$("#list-anak").flexisel({
	    		visibleItems: 4,
	    		clone:false,
			    enableResponsiveBreakpoints: true,
			    responsiveBreakpoints: {
			      portrait: {
			        changePoint:480,
			        visibleItems: 1
			      }, 
			      landscape: {
			        changePoint:640,
			        visibleItems: 2
			      },
			      tablet: {
			        changePoint:768,
			        visibleItems: 3
			      }
			    }
	    	});
		});
	});
</script>