<?php include('header.php'); ?>

<body class="fun-corner">

<?php include('menu.php'); ?>

<div class="wrapper">
	<div class="container">
		<div class="content">
			<div class="row">
				<div class="col-lg-9 list-container">
					<ul id="list-bermain">
						<li>
							<div class="box-image popup">
								<div class="thumb" style="background: url(assets/img/fun-frame.png) no-repeat;"><img src="assets/img/fun-1.png"></div>
							</div>
			                <div class="button-link popup"><a href="#">BERMAIN</a></div>
						</li>
						<li>
							<div class="box-image popup">
								<div class="thumb" style="background: url(assets/img/fun-frame.png) no-repeat;"><img src="assets/img/fun-1.png"></div>
							</div>
			                <div class="button-link popup"><a href="#">BERMAIN</a></div>
						</li>
						<li>
							<div class="box-image popup">
								<div class="thumb" style="background: url(assets/img/fun-frame.png) no-repeat;"><img src="assets/img/fun-1.png"></div>
							</div>
			                <div class="button-link popup"><a href="#">BERMAIN</a></div>
						</li>
						<li>
							<div class="box-image popup">
								<div class="thumb" style="background: url(assets/img/fun-frame.png) no-repeat;"><img src="assets/img/fun-1.png"></div>
							</div>
			                <div class="button-link popup"><a href="#">BERMAIN</a></div>
						</li>
						<li>
							<div class="box-image popup">
								<div class="thumb" style="background: url(assets/img/fun-frame.png) no-repeat;"><img src="assets/img/fun-1.png"></div>
							</div>
			                <div class="button-link popup"><a href="#">BERMAIN</a></div>
						</li>
						<li>
							<div class="box-image popup">
								<div class="thumb" style="background: url(assets/img/fun-frame.png) no-repeat;"><img src="assets/img/fun-1.png"></div>
							</div>
			                <div class="button-link popup"><a href="#">BERMAIN</a></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="popupbox" id="popup-fun">
	<div class="content">
		<div class="row">
			<div class="col-lg-9">
				<div class="row" id="fun-container">
					<div class="col-lg-12">Siapa nama karakter kodomo yang berambut merah?</div>
					<div class="col-lg-12">
						<form id="myform">
							<div class="radio-answer">
								<input type="radio" id="answer-1" name="answer" value="Koko" /><label for="answer-1">Koko</label>
							</div>
							<div class="radio-answer">
								<input type="radio" id="answer-2" name="answer" value="Momo" /><label for="answer-2">Momo</label>
							</div>
							<div class="radio-answer">
								<input type="radio" id="answer-3" name="answer" value="Cici" /><label for="answer-3">Cici</label>
							</div>
							<div class="radio-answer">
								<input type="radio" id="answer-4" name="answer" value="Lion" /><label for="answer-4">Lion</label>
							</div>
						</form>
					</div>
					<div class="col-lg-12 button-link" id="jawab"><a href="#">Jawab</a></div>
				</div>
				<div class="row" id="message" style="display:none;">
					<div class="col-lg-12 result"></div>
					<div class="col-lg-12 button-link" id="bermain"><a href="#">Bermain</a></div>
				</div>
			</div>
		</div>
	</div>
	<a class="close-popup" href="#"></a>
</div>

<?php include('footer.php'); ?>

<script type="text/javascript">
	var status = 0;
	function hidePopup(){
		$('#embbed, #gameplayer-publisher').remove();
		$('#popup-fun .content').show();
		$('#message').hide();
		$('#fun-container').show();
		$('#fade, .popupbox').fadeOut();
		$('#fade').remove();
		$('.close-popup').css({'top': '', 'right': ''});
		status = 0;
	}
	$(document).ready(function(){
		$('#navbar ul.menu li.has-sub').hover(function(){
	      $(this).toggleClass('expanded');
	    });

	    $('.popup').click(function() {
			$('#popup-fun').fadeIn();

			$('body').append('<div id="fade" onClick="hidePopup(); return false;"></div>');
			$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn();

			var popuptopmargin = ($('#popup-fun').height() + 10) / 2;
			var popupleftmargin = ($('#popup-fun').width() + 10) / 2;

			$('#popup-fun').css({
				'margin-top' : -popuptopmargin,
				'margin-left' : -popupleftmargin
			});
		});

		$('.close-popup').click(function(){
			hidePopup();
			return false;
		});

	    $(window).load(function() {
			$("#list-bermain").flexisel({
	    		visibleItems: 4,
	    		clone:false,
			    enableResponsiveBreakpoints: true,
			    responsiveBreakpoints: {
			      portrait: {
			        changePoint:480,
			        visibleItems: 1
			      }, 
			      landscape: {
			        changePoint:640,
			        visibleItems: 2
			      },
			      tablet: {
			        changePoint:768,
			        visibleItems: 3
			      }
			    }
	    	});
		});

		$('#jawab').click(function(){
			var answer = $('#myform input[name="answer"]:checked').val();
			if(answer == 'Koko'){
				$('#popup-fun #message .result').html('Selamat! Tekan tombol bermain untuk melanjutkan permainan!');
				$('#bermain a').html('Mari Bermain!');
				status = 1;
			}
			else{
				$('#popup-fun #message .result').html('Maaf jawaban anda salah');
				$('#bermain a').html('Bermain lagi!');
			}
			$('#message').show();
			$('#fun-container').hide();
			return false;
		});

		$('#bermain a').click(function(){
			if(status == 0){
				$('#message').hide();
				$('#fun-container').show();
				return false;
			}
			$('#popup-fun').append('<div id="embbed" class="gameplayer" data-sub="cdn" data-width="100%" data-height="100%" data-gid="576742227280287722" data-use-viewport="true"></div>');
			(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "http://cdn.gameplayer.io/api/js/publisher.js"; fjs.parentNode.insertBefore(js, fjs);}(document, "script", "gameplayer-publisher"));
			$('#popup-fun .content').hide();
			$('.close-popup').css({ 'right': '-20px', 'top': '-25px' });
			return false;
		});
	});
</script>


